//
//  AppDelegate.m
//  gittie
//
//  Created by Keith Elliott on 5/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"

#import "Parse/Parse.h"
#import "LoginViewController.h"
#import "GittieSigninViewController.h"
#import "DiscoverViewController.h"
#import "FavoritesViewController.h"
#import "GittieViewController.h"
#import "UploadViewController.h"
#import "ProfileViewController.h"
#import "GittieTabBarViewController.h"

@implementation AppDelegate
{
    LoginViewController* login;
    GittieViewController* gittieVC;
    FavoritesViewController* favoritesVC;
    DiscoverViewController* discoverVC;
    UploadViewController* uploadVC;
    ProfileViewController* profileVC;
}
@synthesize window = _window;
@synthesize tabBarController = _tabBarController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // include Parse framework
    [Parse setApplicationId:@"7LnTUFng03WiLAa7JvIFSzDyhGu1nqqXJRtfCMWQ" clientKey:@"kmcKtZWjwhFUy50Ts3sQErF8bKZmAL9fxEIjJQxk"];
    [PFFacebookUtils initializeWithApplicationId:@"450097798349412"];
    [PFTwitterUtils initializeWithConsumerKey:@"RUcn40H8wNvzRQtcymoglg"
                               consumerSecret:@"aSi4g7AshYeXQDAVvSnZ6XCPXJO6LpwZw84bXA"];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    favoritesVC = [[FavoritesViewController alloc] initWithNibName:@"FavoritesViewController" bundle:nil];
    discoverVC = [[DiscoverViewController alloc] initWithNibName:@"DiscoverViewController" bundle:nil];
    gittieVC = [[GittieViewController alloc] initWithNibName:@"GittieViewController" bundle:nil];
    uploadVC = [[UploadViewController alloc] initWithNibName:@"UploadViewController" bundle:nil];
    profileVC = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];

    favoritesVC.tabBarItem.title = @"Favorites";
    favoritesVC.tabBarItem.image = [UIImage imageNamed:@"icon-heart.png"];
    
    discoverVC.tabBarItem.title = @"Discover";
    discoverVC.tabBarItem.image = [UIImage imageNamed:@"71-compass.png"];
    uploadVC.tabBarItem.title = @"Upload";
    uploadVC.tabBarItem.image = [UIImage imageNamed:@"icon-camera2.png"];
    profileVC.tabBarItem.title = @"Me";
    profileVC.tabBarItem.image = [UIImage imageNamed:@"icon-profile.png"];
    
    self.tabBarController = [[GittieTabBarViewController alloc] init];
    self.tabBarController.viewControllers = [NSArray arrayWithObjects:favoritesVC, discoverVC, gittieVC, uploadVC, profileVC, nil];
    self.tabBarController.delegate = self;
    self.window.rootViewController = self.tabBarController;
    [self.window makeKeyAndVisible];
    
    [PFUser logOut];
    if ([PFUser currentUser].isAuthenticated) {

    }
    else {
        login = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        [self.tabBarController presentViewController:login animated:NO completion:nil];
    }

    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    [PFFacebookUtils handleOpenURL:url];
    return YES;
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    [PFFacebookUtils handleOpenURL:url];
    return YES;
}

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
}
*/

/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed
{
}
*/

@end
