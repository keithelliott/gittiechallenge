//
//  SigninViewController.m
//  gittie
//
//  Created by Keith Elliott on 5/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SigninViewController.h"
#import "Parse/Parse.h"

@interface SigninViewController ()

@end

@implementation SigninViewController
@synthesize usernameTF;
@synthesize passwordTF;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    usernameTF.delegate = self;
    passwordTF.delegate = self;
}

- (void)viewDidUnload
{
    [self setUsernameTF:nil];
    [self setPasswordTF:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == usernameTF){
        [passwordTF becomeFirstResponder];
    }
    else{
        [passwordTF resignFirstResponder];
    }
    
    return YES;
}

-(IBAction)signIn:(id)sender
{
    [PFUser logInWithUsernameInBackground:usernameTF.text password:passwordTF.text block:^(PFUser *user, NSError *error){
        
        if(!error){
            [self.view removeFromSuperview];
            [delegate SigninComplete:self];
        }
        else{
            NSLog(@"error logging user");
            [[[UIAlertView alloc] initWithTitle:@"Error logging in"
                                        message:@"Could not log you. Incorrect username or password"
                                       delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil ] show];
        }
    }];
}


@end
