//
//  GittieTabBarViewController.m
//  gittie
//
//  Created by Keith Elliott on 5/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GittieTabBarViewController.h"

@interface GittieTabBarViewController ()

@end

@implementation GittieTabBarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [self addCenterButtonWithImage:[UIImage imageNamed:@"gittieTab.png"] AndHighlightImage:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) addCenterButtonWithImage:(UIImage *)buttonImage AndHighlightImage:(UIImage *)highlightImage{
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    
    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width , buttonImage.size.height);
    [button setImage:buttonImage forState:UIControlStateNormal];
    [button setImage:highlightImage forState:UIControlStateHighlighted];
    
    CGFloat heightDifference = buttonImage.size.height - self.tabBar.frame.size.height;
    if(heightDifference < 0)
        button.center = self.tabBar.center;
    else{
        CGPoint center = self.tabBar.center;
        center.y = self.tabBar.center.y - heightDifference/2;
        button.center = center;
    }
    
    [self.view addSubview:button];
}

@end
