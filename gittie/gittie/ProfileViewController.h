//
//  ProfileViewController.h
//  gittie
//
//  Created by Keith Elliott on 5/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Parse/Parse.h"

@interface ProfileViewController : UIViewController<PF_FBRequestDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *userFullName;
@property (weak, nonatomic) IBOutlet UILabel *location;
@property (weak, nonatomic) IBOutlet UILabel *totalChallenges;
@property (weak, nonatomic) IBOutlet UILabel *totalLikes;
@property (weak, nonatomic) IBOutlet UILabel *totalDislikes;
@property (weak, nonatomic) IBOutlet UILabel *facebookAccount;
@property (weak, nonatomic) IBOutlet UILabel *twitterAccount;
@property (weak, nonatomic) IBOutlet UILabel *emailAddress;
@property (weak, nonatomic) IBOutlet UIButton *facebookAddBtn;
@property (weak, nonatomic) IBOutlet UIButton *twitterAddBtn;
@property (weak, nonatomic) IBOutlet UIButton *emailAddBtn;
@property (weak, nonatomic) IBOutlet UITableView *challengesTableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *logoutBtn;

-(IBAction)logout:(id)sender;
-(IBAction)linkFacebook:(id)sender;
-(IBAction)linkTwitter:(id)sender;
-(IBAction)linkEmail:(id)sender;
@end
