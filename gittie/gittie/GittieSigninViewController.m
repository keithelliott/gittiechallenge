//
//  GittieSigninViewController.m
//  gittie
//
//  Created by Keith Elliott on 5/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GittieSigninViewController.h"
#import "Parse/Parse.h"

@interface GittieSigninViewController ()

@end

@implementation GittieSigninViewController
@synthesize usernameTF;
@synthesize emailTF;
@synthesize passwordTF;
@synthesize signinButton;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    usernameTF.delegate = self;
    passwordTF.delegate = self;
    emailTF.delegate = self;
}

- (void)viewDidUnload
{
    [self setUsernameTF:nil];
    [self setEmailTF:nil];
    [self setPasswordTF:nil];
    [self setSigninButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == usernameTF){
        [emailTF becomeFirstResponder];
    }
    else if(textField == emailTF){
        [passwordTF becomeFirstResponder];
    }
    else{
        [passwordTF resignFirstResponder];
    }
return YES;
}

-(IBAction)signinButtonTapped:(id)sender
{
    PFUser* user = [PFUser currentUser];
    user.username = usernameTF.text;
    user.email = emailTF.text;
    user.password = passwordTF.text;
    
    if([PFUser currentUser].isAuthenticated){
        [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if(!error){
                [self.view removeFromSuperview];
                [delegate SigninComplete:self]; 
            }
            else{
                NSLog(@"error updating user");
            }
        }];
    }
    else{
     [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
         if(!error){
             [self.view removeFromSuperview];
             [delegate SigninComplete:self]; 
         }
         else{
             NSLog(@"error signing in or up user");
             [[[UIAlertView alloc] initWithTitle:@"Error logging in"
                        message:@"Email and/or Password is incorrect"
                        delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil ] show];
         }
     }]; 
    }
    
    [self.view removeFromSuperview];
    [delegate SigninComplete:self];
}

@end
