//
//  LoginViewController.h
//  gittie
//
//  Created by Keith Elliott on 5/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GittieSigninViewController.h"

@interface LoginViewController : UIViewController<LoginCompleteDelegate>
@property (weak, nonatomic) IBOutlet UIButton *gittieSigninButton;
@property (weak, nonatomic) IBOutlet UIButton *facebookSigninButton;
@property (weak, nonatomic) IBOutlet UIButton *twitterSigninButton;

-(IBAction)gittieSigninTapped:(id)sender;
-(IBAction)facebookTapped:(id)sender;
-(IBAction)twitterTapped:(id)sender;
-(void)SigninComplete:(id)parent;

@end
