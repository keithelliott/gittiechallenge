//
//  SigninViewController.h
//  gittie
//
//  Created by Keith Elliott on 5/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GittieSigninViewController.h"

@interface SigninViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *usernameTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) id<LoginCompleteDelegate> delegate;

-(IBAction)signIn:(id)sender;
@end
