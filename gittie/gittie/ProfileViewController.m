//
//  ProfileViewController.m
//  gittie
//
//  Created by Keith Elliott on 5/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()
{
}
@end

@implementation ProfileViewController
@synthesize profileImage;
@synthesize userFullName;
@synthesize location;
@synthesize totalChallenges;
@synthesize totalLikes;
@synthesize totalDislikes;
@synthesize facebookAccount;
@synthesize twitterAccount;
@synthesize emailAddress;
@synthesize facebookAddBtn;
@synthesize twitterAddBtn;
@synthesize emailAddBtn;
@synthesize challengesTableView;
@synthesize logoutBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    PFUser* user = [PFUser currentUser];
    if(user.sessionToken){
            PF_Facebook* facebook = [PFFacebookUtils facebook];
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           @"name, link, location, picture",  @"fields",
                                           nil];
            [facebook requestWithGraphPath:@"me" andParams:params andDelegate:self];
    }
     
}

- (void)viewDidUnload
{
    [self setProfileImage:nil];
    [self setUserFullName:nil];
    [self setLocation:nil];
    [self setTotalChallenges:nil];
    [self setTotalLikes:nil];
    [self setTotalDislikes:nil];
    [self setFacebookAccount:nil];
    [self setTwitterAccount:nil];
    [self setEmailAddress:nil];
    [self setFacebookAddBtn:nil];
    [self setTwitterAddBtn:nil];
    [self setEmailAddBtn:nil];
    [self setChallengesTableView:nil];
    [self setLogoutBtn:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(IBAction)logout:(id)sender
{
    [PFUser logOut];
    
}

-(IBAction)linkFacebook:(id)sender
{
     PFUser* user = [PFUser currentUser];
    NSArray *permissions = [[NSArray alloc] initWithObjects:
                            @"user_about_me",@"user_photos",
                            @"user_location",
                            @"offline_access", nil];

    [PFFacebookUtils linkUser:user permissions:permissions block:^(BOOL succeeded, NSError *error) {
        if(!error){
            if(succeeded)
            {
                PF_Facebook* facebook = [PFFacebookUtils facebook];
                NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                               @"name, link, location, picture",  @"fields",
                                               nil];
                [facebook requestWithGraphPath:@"me" andParams:params andDelegate:self]; 
            }
        }
    }];
}

-(void)request:(PF_FBRequest *)request didLoad:(id)result
{
    facebookAddBtn.hidden = YES;
    userFullName.text = [result valueForKey:@"name"];
    facebookAccount.text = [result valueForKey:@"link"];
    location.text = [[result valueForKey:@"location"] valueForKey:@"name"];
    NSString* picture = [result valueForKey:@"picture"];
    
    NSString * urlString = [picture stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    NSURL * imageURL = [NSURL URLWithString:urlString];
    NSData * imageData = [NSData dataWithContentsOfURL:imageURL];
    UIImage * image = [UIImage imageWithData:imageData];
    profileImage.image = image;
}

-(IBAction)linkTwitter:(id)sender
{
    PFUser* user = [PFUser currentUser];
    if (![PFTwitterUtils isLinkedWithUser:user]) {
        [PFTwitterUtils linkUser:user block:^(BOOL succeeded, NSError *error) {
            if ([PFTwitterUtils isLinkedWithUser:user]) {
                NSLog(@"Woohoo, user logged in with Twitter!");
                twitterAddBtn.hidden = YES;
//                NSURL *verify = [NSURL URLWithString:@"https://api.twitter.com/1/account/verify_credentials.json"];
//                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:verify];
//                [[PFTwitterUtils twitter] signRequest:request];
//                NSURLResponse *response = nil;
//                NSData *data = [NSURLConnection sendSynchronousRequest:request
//                                                     returningResponse:&response
//                                                                 error:&error];
            }
        }];
    }
}

-(IBAction)linkEmail:(id)sender
{
    
}


-(void)viewDidAppear:(BOOL)animated{
    PFUser* user = [PFUser currentUser];
    
    if(user.isAuthenticated){
        userFullName.text = user.username;
        emailAddress.text = user.email;
        emailAddBtn.hidden = YES;
        
        
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
