//
//  GittieTabBarViewController.h
//  gittie
//
//  Created by Keith Elliott on 5/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GittieTabBarViewController : UITabBarController

- (void) addCenterButtonWithImage:(UIImage *)buttonImage AndHighlightImage:(UIImage *)highlightImage;
@end
