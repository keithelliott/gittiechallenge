//
//  LoginViewController.m
//  gittie
//
//  Created by Keith Elliott on 5/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LoginViewController.h"
#import "Parse/Parse.h"
#import "SigninViewController.h"

@interface LoginViewController ()
{
    GittieSigninViewController* signup;
    SigninViewController* signin;
}
@end

@implementation LoginViewController
@synthesize gittieSigninButton;
@synthesize facebookSigninButton;
@synthesize twitterSigninButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [self setGittieSigninButton:nil];
    [self setFacebookSigninButton:nil];
    [self setTwitterSigninButton:nil];
    [super viewDidUnload];
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction)gittieSigninTapped:(id)sender
{
    signin = [[SigninViewController alloc]initWithNibName:@"SigninViewController" bundle:nil];
    signin.delegate = self;
    [self.view addSubview:signin.view];
}

-(void)SigninComplete:(id)parent
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)facebookTapped:(id)sender
{
    NSArray *permissions = [[NSArray alloc] initWithObjects:
                            @"user_about_me",@"user_photos",
                            @"user_location",
                            @"offline_access", nil];
    
    [PFFacebookUtils logInWithPermissions:permissions block:^(PFUser *user, NSError *error) {
        if(!error){
            PFQuery *query = [PFQuery queryForUser];
            [query getObjectInBackgroundWithId:user.objectId block:^(PFObject *object, NSError *suberror) {
                if(!suberror){
                    if(object.objectId != nil){
                        NSLog(@"Found user");
                        [self SigninComplete:self];
                    }
                    else{
                        NSLog(@"User doesn't exist");
                        signup = [[GittieSigninViewController alloc]initWithNibName:@"GittieSigninViewController" bundle:nil];
                        signup.delegate = self;
                        [self.view addSubview:signup.view];
                    }
                }
                else{
                    NSLog(@"%@",suberror.debugDescription);
                }
            }];
        }
        else{
            NSLog(@"Error logging in with facebook");
            [[[UIAlertView alloc] initWithTitle:@"Error logging in"
                                        message:@"Could not log you in using your Facebook account"
                                       delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil ] show];
        }
    }]; 
}

-(IBAction)twitterTapped:(id)sender
{
    [PFTwitterUtils logInWithBlock:^(PFUser *user, NSError *error) {
        if (!user) {
            NSLog(@"The user cancelled the Twitter login.");
            return;
        } else if (user.isNew) {
            NSLog(@"User signed up and logged in with Twitter!");
            signup = [[GittieSigninViewController alloc]initWithNibName:@"GittieSigninViewController" bundle:nil];
            signup.delegate = self;
            [self.view addSubview:signup.view];
        } else {
            NSLog(@"User logged in with Twitter!");
            [self.view addSubview:signup.view];
        }     
    }];
}
@end
