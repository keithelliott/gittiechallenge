//
//  GittieSigninViewController.h
//  gittie
//
//  Created by Keith Elliott on 5/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LoginCompleteDelegate <NSObject>
-(void)SigninComplete:(id)parent;
@end

@interface GittieSigninViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *usernameTF;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UIButton *signinButton;
@property (weak, nonatomic) id<LoginCompleteDelegate> delegate;

-(IBAction)signinButtonTapped:(id)sender;
@end
